//
//  ViewController.m
//  p02-montgomery
//
//  Created by Montgomery Chelsea on 2/4/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import "ViewController.h"
#include <stdlib.h>

@interface ViewController ()

@end

@implementation ViewController

//Synthesize all the buttons and labels so we can use them in here

// Debugging variable
bool debug = false;

//Labels
@synthesize sq_1;
@synthesize sq_2;
@synthesize sq_3;
@synthesize sq_4;
@synthesize sq_5;
@synthesize sq_6;
@synthesize sq_7;
@synthesize sq_8;
@synthesize sq_9;
@synthesize sq_10;
@synthesize sq_11;
@synthesize sq_12;
@synthesize sq_13;
@synthesize sq_14;
@synthesize sq_15;
@synthesize sq_16;
@synthesize current_score;
@synthesize high_score;

//Buttons
@synthesize up_button;
@synthesize down_button;
@synthesize left_button;
@synthesize right_button;
@synthesize restart_button;

//Current and high scores
int current_score_num;
long high_score_num;

//Game over label
@synthesize game_over_label;

//Label array for number squares
NSArray *labelArray;


//Defining colors for use depending on the number in the square
-(UIColor*) empty_square_color
{
    return [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1];
}
-(UIColor*) two_square_color
{
    return [UIColor colorWithRed:204.0/255.0 green:230.0/255.0 blue:255.0/255.0 alpha:1];
}
-(UIColor*) four_square_color
{
    return [UIColor colorWithRed:153.0/255.0 green:206.0/255.0 blue:255.0/255.0 alpha:1];
}
-(UIColor*) eight_square_color
{
    return [UIColor colorWithRed:102.0/255.0 green:181.0/255.0 blue:255.0/255.0 alpha:1];
}
-(UIColor*) sixteen_square_color
{
    return [UIColor colorWithRed:51.0/255.0 green:156.0/255.0 blue:255.0/255.0 alpha:1];
}
-(UIColor*) thirty_two_square_color
{
    return [UIColor colorWithRed:0.0/255.0 green:132.0/255.0 blue:255.0/255.0 alpha:1];
}
-(UIColor*) sixty_four_square_color
{
    return [UIColor colorWithRed:215.0/255.0 green:179.0/255.0 blue:255.0/255.0 alpha:1];
}
-(UIColor*) one_twenty_eight_square_color
{
    return [UIColor colorWithRed:187.0/255.0 green:128.0/255.0 blue:255.0/255.0 alpha:1];
}
-(UIColor*) two_fifty_six_square_color
{
    return [UIColor colorWithRed:160.0/255.0 green:77.0/255.0 blue:255.0/255.0 alpha:1];
}
-(UIColor*) five_twelve_square_color
{
    return [UIColor colorWithRed:153.0/255.0 green:255.0/255.0 blue:201.0/255.0 alpha:1];
}
-(UIColor*) ten_twenty_four_square_color
{
    return [UIColor colorWithRed:102.0/255.0 green:255.0/255.0 blue:173.0/255.0 alpha:1];
}
-(UIColor*) twenty_forty_eight_square_color
{
    return [UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:95.0/255.0 alpha:1];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Reset the current score
    current_score_num = 0;
    
    //Place all of the labels into an array
    labelArray = [NSArray arrayWithObjects:sq_1, sq_2, sq_3, sq_4, sq_5, sq_6, sq_7, sq_8, sq_9,
                           sq_10, sq_11, sq_12, sq_13, sq_14, sq_15, sq_16, nil];
    
    //Set borders for all labels, empty out labels to start
    for(int i = 0; i<16; ++i)
    {
        UILabel *cur_label = labelArray[i];
        cur_label.layer.borderColor = [UIColor blackColor].CGColor;
        cur_label.layer.borderWidth = 2.0f;
        [cur_label setText:@""];
    }
    
    //Choose two random labels to begin with numbers
    int rand_1 = arc4random_uniform(16);
    int rand_2 = arc4random_uniform(16);
    //Just in case we get two of the same number
    while(rand_1 == rand_2)
    {
        rand_2 = arc4random_uniform(16);
    }
    if ( debug )
    {
        NSLog(@"%d",rand_1);
        NSLog(@"%d",rand_2);
        //rand_1 = 11;
        //rand_2 = 3;
    }
    
    //Set the two random labels to '2' to begin
    UILabel *rand_label_1 = labelArray[rand_1];
    UILabel *rand_label_2 = labelArray[rand_2];
    [rand_label_1 setText:@"2"];
    [rand_label_2 setText:@"2"];
    if(debug)
    {
        //UILabel *test = labelArray[15];
        //[test setText:@"8"];
    }
    
    //Get the saved user high score
    high_score_num = [[NSUserDefaults standardUserDefaults] integerForKey: @"highScore"];
    
    //Convert to string to put on label
    NSString* high_score_string = [NSString stringWithFormat:@"%li", high_score_num];
    NSString* cur_score_string = [NSString stringWithFormat:@"%i", current_score_num];
    
    //Set the high and current score
    [high_score setText:high_score_string];
    [current_score setText:cur_score_string];
    
    //Hide the game over label
    [game_over_label setHidden:YES];
    
    //color boxes appropriately
    [self update_colors];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Function to update the colors of the labels based on their number
-(void)update_colors
{
    if(debug)
    {
        NSLog(@"updating all colors");
    }
    
    for(int i = 0; i<16; ++i)
    {
        UILabel *color_label = labelArray[i];
        
        if([color_label.text isEqual:@""])
        {
            color_label.backgroundColor = [self empty_square_color];
            continue;
        }
        
        //get the value of the square mod 2048 to determine what to color it
        long square_val = [color_label.text integerValue];
        square_val = square_val / 2;
        square_val = square_val % 2048;
        if(debug)
        {
            NSLog(@"coloring sq val %ld", square_val);
        }
        
        if(square_val == 1)
        {
            color_label.backgroundColor = [self two_square_color];
        }
        
        else if(square_val == 2)
        {
            color_label.backgroundColor = [self four_square_color];
        }
        
        else if (square_val == 4)
        {
            color_label.backgroundColor = [self eight_square_color];
        }
        
        else if (square_val == 8)
        {
            color_label.backgroundColor = [self sixteen_square_color];
        }
        
        else if (square_val == 16)
        {
            color_label.backgroundColor = [self thirty_two_square_color];
        }
        
        else if (square_val == 32)
        {
            color_label.backgroundColor = [self sixty_four_square_color];
        }
        
        else if (square_val == 64)
        {
            color_label.backgroundColor = [self one_twenty_eight_square_color];
        }
        
        else if (square_val == 128)
        {
            color_label.backgroundColor = [self two_fifty_six_square_color];
        }
        
        else if (square_val == 256)
        {
            color_label.backgroundColor = [self five_twelve_square_color];
        }
        
        else if (square_val == 512)
        {
            color_label.backgroundColor = [self ten_twenty_four_square_color];
        }
        
        else if (square_val == 1024)
        {
            color_label.backgroundColor = [self twenty_forty_eight_square_color];
        }
        
        //color white if we have an error?
        else
        {
            color_label.backgroundColor = [UIColor whiteColor];
        }
    }
}

//Function to save a new highscore if needed
-(void)save_score
{
    // Save the score if the current is higher than the high score
    if(current_score_num > high_score_num)
    {
        high_score_num = current_score_num;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setInteger:high_score_num forKey:@"highScore"];
        [defaults synchronize];
    }
    
    // Set the current and high score labels
    //Convert to string to put on label
    current_score_num = 0;
    NSString* high_score_string = [NSString stringWithFormat:@"%li", high_score_num];
    NSString* cur_score_string = [NSString stringWithFormat:@"%i", current_score_num];
    
    //Set the high and current score
    [high_score setText:high_score_string];
    [current_score setText:cur_score_string];
    
    
}

//Function to randomly insert a number
-(void)insert_new_num
{
    if(debug)
    {
        NSLog(@"inserting random number");
    }
    
    //Check to make sure the game isn't over
    bool over_check = [self game_over_check];
    if(!over_check)
    {
        //Make sure there's a blank space for us to insert, they could have hit a wrong arrow button and there are no blank spaces
        bool looking_for_blank = true;
        int c = 0;
        while(looking_for_blank && c<16)
        {
            UILabel *is_it_blank = labelArray[c];
            
            if([is_it_blank.text isEqual:@""])
            {
                looking_for_blank = false;
                
                if(debug)
                {
                    NSLog(@"ok to add new num found blank at %d", c);
                }
                
                continue;
            }
            
            ++c;
        }
        
        if(!looking_for_blank)
        {
            //Get a random square and make sure nothing is there yet
            int random_square = arc4random_uniform(16);
                UILabel *rs = labelArray[random_square];
            while(![rs.text isEqual:@""])
            {
                random_square = arc4random_uniform(16);
                rs = labelArray[random_square];
            }
        
            if(debug)
            {
                NSLog(@"inserting random at %d", random_square);
            }
        
            [rs setText:@"2"];
        } //end insert new number into blank space
        else
        {
            if(debug)
            {
                NSLog(@"game not over but can't add new number");
            }
        }
        
    } //end game over check
    
    //Update all square colors after adding a number
    [self update_colors];
    
} //End insert new num function

//Function to update the current score of the game
//Score updates by the value of the square created
-(void)updateScore
{
    NSString* cur_score_string = [NSString stringWithFormat:@"%i", current_score_num];
    [current_score setText:cur_score_string];
}

//Function to check if user has any moves or if its game over
-(Boolean)game_over_check
{
    if(debug)
    {
        NSLog(@"doing game over check");
    }
    
    Boolean over = false;
    
    //Ways game could not be over:
    // two squares next to eachother match
    // there is an empty square with 2 or "" on any side of it
    bool can_continue = false;
    int c = 0;
    while(c<16 && !can_continue)
    {
        UILabel *c_label = labelArray[c];
        
        int up=c+1, down=c-1, left=c-4, right=c+4;
        
        if(debug)
        {
            NSLog(@"checking %d, up %d, down %d, left %d, right %d", c, up, down, left, right);
        }
        
        //Only check up if its less than 16, not 4, not 8, not 12
        if(up<16 && up!=4 && up!=8 && up!=12)
        {
            //Get square label up
            UILabel *testing_label = labelArray[up];
            
            //Check if square next to it matches
            if([testing_label.text isEqual:c_label.text])
            {
                if(debug)
                {
                    NSLog(@"up square matches, moving on");
                }
                
                can_continue = true;
                continue;
            }
            
            //If square is empty and adjacent is empty or two then we can continue
            if([c_label.text isEqual:@""] && ([testing_label.text isEqual:@""] || [testing_label.text isEqual:@"2"]))
            {
                if(debug)
                {
                    NSLog(@"square is blank and up is blank or 2 moving on");
                }
                
                can_continue = true;
                continue;
            }
        } //End checking up label
        
        //Only check down if its greater than or equal to 0, not 3, not 7, not 11
        if(down>=0 && down!=3 && down!=7 && down!=11)
        {
            //Get square label down
            UILabel *testing_label = labelArray[down];
            
            //Check if square next to it matches
            if([testing_label.text isEqual:c_label.text])
            {
                if(debug)
                {
                    NSLog(@"down square matches, moving on");
                }
                
                can_continue = true;
                continue;
            }
            
            //If square is empty and adjacent is empty or two then we can continue
            if([c_label.text isEqual:@""] && ([testing_label.text isEqual:@""] || [testing_label.text isEqual:@"2"]))
            {
                if(debug)
                {
                    NSLog(@"square is blank and down is blank or 2 moving on");
                }
                
                can_continue = true;
                continue;
            }
        } //End checking down label
        
        //Only check left if its greater than or equal to 0
        if(left>=0)
        {
            //Get square label down
            UILabel *testing_label = labelArray[left];
            
            //Check if square next to it matches
            if([testing_label.text isEqual:c_label.text])
            {
                if(debug)
                {
                    NSLog(@"left square matches, moving on");
                }
                
                can_continue = true;
                continue;
            }
            
            //If square is empty and adjacent is empty or two then we can continue
            if([c_label.text isEqual:@""] && ([testing_label.text isEqual:@""] || [testing_label.text isEqual:@"2"]))
            {
                if(debug)
                {
                    NSLog(@"square is blank and left is blank or 2 moving on");
                }
                
                can_continue = true;
                continue;
            }
        } //End checking left label
        
        //Only check right label if its less than or equal to 15
        if(right<=15)
        {
            //Get square label down
            UILabel *testing_label = labelArray[right];
            
            if(debug)
            {
                NSLog(@"checking right %@ %@", c_label.text, testing_label.text);
            }
            
            //Check if square next to it matches
            if([testing_label.text isEqual:c_label.text])
            {
                if(debug)
                {
                    NSLog(@"right square matches, moving on");
                }
                
                can_continue = true;
                continue;
            }
            
            //If square is empty and adjacent is empty or two then we can continue
            if([c_label.text isEqual:@""] && ([testing_label.text isEqual:@""] || [testing_label.text isEqual:@"2"]))
            {
                if(debug)
                {
                    NSLog(@"square is blank and right is blank or 2 moving on");
                }
                
                can_continue = true;
                continue;
            }
        } //End checking right label
        
        
        //Move to next square
        ++c;
        
    } //end c<16 && !can_continue
    
    //If we got through all locations and can_continue is still false the game is over
    if(!can_continue)
    {
        if(debug)
        {
            NSLog(@"game is over");
        }
        
        //See if we need to fill last square in with two
        for(int n = 0; n<16; ++n)
        {
            UILabel *is_it_blank = labelArray[n];
            if([is_it_blank.text isEqual:@""])
            {
                [is_it_blank setText:@"2"];
                break;
            }
        }
        
        over = true;
    }
    
    //If the game is over we need to save the score
    if(over)
    {
        [self save_score];
        
        //Disable buttons
        up_button.enabled = NO;
        down_button.enabled = NO;
        left_button.enabled = NO;
        right_button.enabled = NO;
        
        //Display game over message
        [game_over_label setHidden:NO];
    }
    
    return over;
}

//Movement buttons

//Matrix is set up:
// 1   5   9    13
// 2   6   10   14
// 3   7   11   15
// 4   8   12   16


//Up button clicked
-(IBAction)up_click:(id)sender
{
    //check to make sure we only add a new number if we made a vaid move
    bool moved_number = false;
    
    //Have an array telling what squares have already been combined so we don't double combine on a single turn
    NSInteger combined_squares[16];
    for(int i = 0; i<16; ++i)
    {
        combined_squares[i] = 0;
    }
    
    //Begin with bottom row and check if any can be moved up
    for(int i = 16; i>=4; i-=4)
    {
        //Don't want to change i since we use it in the loop
        int col_start = i;
        
        //Go through every option in column
        for(int x = (i-1); x>(i-4); --x)
        {
            //Get the labels we're working with
            int curLabel = col_start-1;
            int checkLabel = x-1;
            UILabel *cur = labelArray[curLabel];
            UILabel *check = labelArray[checkLabel];
            
            
            if(debug)
            {
                NSLog(@"Processing %d and %d ", curLabel+1, checkLabel+1);
                NSLog(@"Values %@, %@", cur.text, check.text);
            }
            
            
            //If our current label is blank we can move on
            if([cur.text isEqual: @""])
            {
                if(debug)
                {
                    NSLog(@"current blank moving on");
                }
                
                --col_start;
                
                continue;
            }
            
            //If the current label isn't blank we have three options
            //Case 1: next is blank, move number
            if([check.text isEqual:@""])
            {
                if(debug)
                {
                    NSLog(@"next is blank moving number");
                }
                
                //When the next is blank we need to move everything in the column up
                int move_down = curLabel;
                for(int a = (curLabel-1); a<(i-1); ++a)
                {
                    if(debug)
                    {
                        NSLog(@"moving %d to %d", move_down, a);
                    }
                    
                    UILabel *move_to_label = labelArray[a];
                    UILabel *move_from_label = labelArray[move_down];
                    [move_to_label setText:move_from_label.text];
                    [move_from_label setText:@""];
                    
                    ++move_down;
                }
                
                moved_number = true;
                --col_start;
                continue;
            }
            //Case 2: next matches, add
            else if([check.text isEqual: cur.text])
            {
                if(debug)
                {
                    NSLog(@"numbers match combining %d, %d", curLabel, checkLabel);
                }
                
                //Make sure we are not double combining a square
                if(combined_squares[curLabel] == 0)
                {
                
                    //Get the combined value
                    long num_1 = [cur.text integerValue];
                    long num_2 = [check.text integerValue];
                    long combined = num_1 + num_2;
                    
                    //Set the box texts
                    [cur setText:@""];
                    NSString *combined_score = [NSString stringWithFormat:@"%ld", combined];
                    [check setText: combined_score];
                
                    //Update the score
                    current_score_num += combined;
                    
                    //Add to combined squares so we don't combine again
                    combined_squares[checkLabel] = 1;
                    
                    //Check if we need to move squares down
                    int move_down = curLabel+1;
                    for(int a = (curLabel); a<(i-1); ++a)
                    {
                        if(debug)
                        {
                            NSLog(@"moving %d to %d", move_down, a);
                        }
                        
                        UILabel *move_to_label = labelArray[a];
                        UILabel *move_from_label = labelArray[move_down];
                        [move_to_label setText:move_from_label.text];
                        [move_from_label setText:@""];
                        
                        ++move_down;
                    }

                }
                
                moved_number = true;
                --col_start;
                continue;
            }
            //Case 3: next does not match
            else
            {
                --col_start;
                continue;
            }
            
            
        } //End of going through column
        
    } //End of moving to next column
    
     //update the score label
    [self updateScore];
    
    //generate random square - game over check is done inside this function
    if(moved_number)
    {
        [self insert_new_num];
    }
        
}

//Down button clicked
-(IBAction)down_click:(id)sender
{
    //only insert new number if we moved
    bool moved_number = false;
    
    if(debug)
    {
        NSLog(@"----DOWN CLICKED----");
    }
    
    //Have an array telling what squares have already been combined so we don't double combine on a single turn
    NSInteger combined_squares[16];
    for(int i = 0; i<16; ++i)
    {
        combined_squares[i] = 0;
    }
    
    //Start with top row and see if we can move any down
    for(int i = 1; i<=16; i+=4)
    {
        //Don't want to change i since we use it in the loop
        int col_start = i;
        
        //Go through every option the column
        for(int x = (i+1); x<(i+4); ++x)
        {
            //Get the labels we're working with
            int curLabel = col_start-1;
            int checkLabel = x-1;
            UILabel *cur = labelArray[curLabel];
            UILabel *check = labelArray[checkLabel];
            
            if(debug)
            {
                NSLog(@"Processing %d and %d ", curLabel+1, checkLabel+1);
                NSLog(@"Values %@, %@", cur.text, check.text);
            }
            
            //If our current label is blank we can move on
            if([cur.text isEqual: @""])
            {
                if(debug)
                {
                    NSLog(@"current blank moving on");
                }
                
                ++col_start;
                
                continue;
            }
            
            //If the current label isn't blank we have three options
            //Case 1: next is blank, move number
            if([check.text isEqual:@""])
            {
                if(debug)
                {
                    NSLog(@"next is blank moving number");
                }
                
                //When the next is blank we need to move everything in the column down
                int moving = curLabel;
                for(int a = checkLabel; a>(i-1); --a)
                {
                    if(debug)
                    {
                        NSLog(@"moving %d to %d", moving, a);
                    }
                    
                    UILabel *move_to = labelArray[a];
                    UILabel *move_from = labelArray[moving];
                    
                    [move_to setText:move_from.text];
                    [move_from setText:@""];
                    
                    --moving;
                }
                
                moved_number = true;
                ++col_start;
                continue;

            }
            //Case 2: next matches, add
            else if([check.text isEqual: cur.text])
            {
                if(debug)
                {
                    NSLog(@"numbers match combining %d, %d", curLabel, checkLabel);
                }
                
                //Make sure we are not double combining a square
                if(combined_squares[curLabel] == 0)
                {
                    
                    //Get the combined value
                    long num_1 = [cur.text integerValue];
                    long num_2 = [check.text integerValue];
                    long combined = num_1 + num_2;
                    
                    //Set the box texts
                    [cur setText:@""];
                    NSString *combined_score = [NSString stringWithFormat:@"%ld", combined];
                    [check setText: combined_score];
                    
                    //Update the score
                    current_score_num += combined;
                    
                    //Add to combined squares so we don't combine again
                    combined_squares[checkLabel] = 1;
                    
                    //Check if we need to move squares down
                    int moving = curLabel-1;
                    for(int a = curLabel; a>(i-1); --a)
                    {
                        if(debug)
                        {
                            NSLog(@"moving %d to %d", moving, a);
                        }
                        
                        UILabel *move_to = labelArray[a];
                        UILabel *move_from = labelArray[moving];
                        
                        [move_to setText:move_from.text];
                        [move_from setText:@""];
                        
                        --moving;
                    }
                    
                }
                
                moved_number = true;
                ++col_start;
                continue;


            }
            //Case 3: next does not match
            else
            {
                if(debug)
                {
                    NSLog(@"do not match");
                }
                
                ++col_start;
                continue;
            }
            
        } //Finished going through columns
        
    } //Finished going through rows
    
    //Update the score
    [self updateScore];
    
    //generate random square - game over check is done inside this function
    if(moved_number)
    {
        [self insert_new_num];
    }
}

//Left button clicked
-(IBAction)left_click:(id)sender
{
    //only insert new if we moved a number
    bool moved_number = false;
    
    if(debug)
    {
        NSLog(@"----LEFT CLICKED----");
    }
    
    //Have an array telling what squares have already been combined so we don't double combine on a single turn
    NSInteger combined_squares[16];
    for(int i = 0; i<16; ++i)
    {
        combined_squares[i] = 0;
    }
    
    //Go through columns starting from the right and combine
    for(int i=16; i>=13; --i)
    {
        
        //dont want to change i since we use that in the loop
        int row_start = i;
        
        //go through every item in the row
        for(int x = (i-4); x>=(i-12); x-=4)
        {
            //Get the labels we're working with
            int curLabel = row_start-1;
            int checkLabel = x-1;
            UILabel *cur = labelArray[curLabel];
            UILabel *check = labelArray[checkLabel];
            
            if(debug)
            {
                NSLog(@"Processing %d and %d ", curLabel+1, checkLabel+1);
                NSLog(@"Values %@, %@", cur.text, check.text);
            }
            
            //If our current label is blank we can move on
            if([cur.text isEqual: @""])
            {
                if(debug)
                {
                    NSLog(@"current blank moving on");
                }
                
                row_start-=4;
                continue;
                
            }
            
            //If the current label isn't blank we have three options
            //Case 1: next is blank, move number
            if([check.text isEqual:@""])
            {
                if(debug)
                {
                    NSLog(@"next is blank moving number");
                }
                
                int moving = curLabel;
                for(int a = checkLabel; a<(i-1); a+=4)
                {
                    if(debug)
                    {
                        NSLog(@"moving %d to %d", moving, a);
                    }
                    
                    UILabel *move_from = labelArray[moving];
                    UILabel *move_to = labelArray[a];
                    
                    [move_to setText:move_from.text];
                    [move_from setText:@""];
                    
                    moving+=4;
                }
                
                moved_number = true;
                row_start-=4;
                continue;
                
            }
            //Case 2: next matches, add
            else if([check.text isEqual: cur.text])
            {
                if(debug)
                {
                    NSLog(@"numbers match combining %d, %d", curLabel, checkLabel);
                }
                
                //Make sure we are not double combining a square
                if(combined_squares[curLabel] == 0)
                {
                    
                    //Get the combined value
                    long num_1 = [cur.text integerValue];
                    long num_2 = [check.text integerValue];
                    long combined = num_1 + num_2;
                    
                    //Set the box texts
                    [cur setText:@""];
                    NSString *combined_score = [NSString stringWithFormat:@"%ld", combined];
                    [check setText: combined_score];
                    
                    //Update the score
                    current_score_num += combined;
                    
                    //Add to combined squares so we don't combine again
                    combined_squares[checkLabel] = 1;
                    
                    //Check to see if we need to move any squares over
                    int moving = curLabel+4;
                    if(debug)
                    {
                        NSLog(@"combining left val of moving is %d", moving);
                    }
                    for(int a = curLabel; a<(i-1); a+=4)
                    {
                        if(moving<16)
                        {
                            if(debug)
                            {
                                NSLog(@"combined now moving %d to %d", moving, a);
                            }
                        
                            UILabel *move_to = labelArray[a];
                            UILabel *move_from = labelArray[moving];
                        
                            [move_to setText:move_from.text];
                            [move_from setText:@""];
                        
                            moving+=4;
                        }
                    }

                    
                }
                
                moved_number = true;
                row_start-=4;
                continue;
            }
            //Case 3: next does not match
            else
            {
                if(debug)
                {
                    NSLog(@"do not match");
                }
                
                row_start-=4;
                continue;

            }
            
            
        }
        
    }

    //update the score label
    [self updateScore];
    
    //generate random square - game over check is done inside this function
    if(moved_number)
    {
        [self insert_new_num];
    }
}

//Right button clicked
-(IBAction)right_click:(id)sender
{
    
    //only insert new number if we moved
    bool moved_number = false;
    
    if(debug)
    {
        NSLog(@"----RIGHT CLICKED----");
    }
    
    //Have an array telling what squares have already been combined so we don't double combine on a single turn
    NSInteger combined_squares[16];
    for(int i = 0; i<16; ++i)
    {
        combined_squares[i] = 0;
    }
    
    //Go through each column starting from the left and pushing over
    for(int i = 4; i>0; --i)
    {
        //dont want to change i since we use that in the loop
        int row_start = i;
        
        //go through each item in the row
        for(int x = i+4; x<=i+12; x+=4)
        {
            //Get the labels we're working with
            int curLabel = row_start-1;
            int checkLabel = x-1;
            UILabel *cur = labelArray[curLabel];
            UILabel *check = labelArray[checkLabel];
            
            if(debug)
            {
                NSLog(@"Processing %d and %d ", curLabel+1, checkLabel+1);
                NSLog(@"Values %@, %@", cur.text, check.text);
            }
            
            //If our current label is blank we can move on
            if([cur.text isEqual: @""])
            {
                if(debug)
                {
                    NSLog(@"current blank moving on");
                }
                
                row_start+=4;
                continue;
                
            }
            
            //If the current label isn't blank we have three options
            //Case 1: next is blank, move number
            if([check.text isEqual:@""])
            {
                if(debug)
                {
                    NSLog(@"next is blank moving number");
                }
                
                int moving = curLabel;
                for(int a = checkLabel; a>(i-1); a-=4)
                {
                    if(debug)
                    {
                        NSLog(@"moving %d to %d", moving, a);
                    }
                    
                    UILabel *move_from = labelArray[moving];
                    UILabel *move_to = labelArray[a];
                    
                    [move_to setText:move_from.text];
                    [move_from setText:@""];
                    
                    moving-=4;
                }
                
                moved_number = true;
                row_start+=4;
                continue;
                
            }
            //Case 2: next matches, add
            else if([check.text isEqual: cur.text])
            {
                if(debug)
                {
                    NSLog(@"numbers match combining %d, %d", curLabel, checkLabel);
                }
                
                //Make sure we are not double combining a square
                if(combined_squares[curLabel] == 0)
                {
                    
                    //Get the combined value
                    long num_1 = [cur.text integerValue];
                    long num_2 = [check.text integerValue];
                    long combined = num_1 + num_2;
                    
                    //Set the box texts
                    [cur setText:@""];
                    NSString *combined_score = [NSString stringWithFormat:@"%ld", combined];
                    [check setText: combined_score];
                    
                    //Update the score
                    current_score_num += combined;
                    
                    //Add to combined squares so we don't combine again
                    combined_squares[checkLabel] = 1;
                    
                    //Check to see if we need to move any squares over
                    int moving = curLabel-4;
                    if(debug)
                    {
                        NSLog(@"combining right val of moving is %d", moving);
                    }
                    for(int a = curLabel; a>(i-1); a-=4)
                    {
                        if(debug)
                        {
                            NSLog(@"combined now moving %d to %d", moving, a);
                        }
                        
                        UILabel *move_to = labelArray[a];
                        UILabel *move_from = labelArray[moving];
                            
                        [move_to setText:move_from.text];
                        [move_from setText:@""];
                            
                        moving-=4;
                    }
                    
                    
                }
                
                moved_number = true;
                row_start+=4;
                continue;
            }
            //Case 3: next does not match
            else
            {
                if(debug)
                {
                    NSLog(@"do not match");
                }
                
                row_start+=4;
                continue;
                
            }

            
        }
    }
    
    //update the score label
    [self updateScore];
    
    //generate random square - game over check is done inside this function
    if(moved_number)
    {
        [self insert_new_num];
    }
}

//Restart button clicked
-(IBAction)restart_click:(id)sender
{
    //Save the score and reset
    [self save_score];
    
    //Enable buttons
    up_button.enabled = YES;
    down_button.enabled = YES;
    left_button.enabled = YES;
    right_button.enabled = YES;
    
    //Display game over message
    [game_over_label setHidden:YES];
    
    //Reset the squares
    for(int i = 0; i<16; ++i)
    {
        UILabel *cur_label = labelArray[i];
        [cur_label setText:@""];
    }
    
    //Choose two random labels to begin with numbers
    int rand_1 = arc4random_uniform(16);
    int rand_2 = arc4random_uniform(16);
    //Just in case we get two of the same number
    while(rand_1 == rand_2)
    {
        rand_2 = arc4random_uniform(16);
    }
    
    //Set the two random labels to '2' to begin
    UILabel *rand_label_1 = labelArray[rand_1];
    UILabel *rand_label_2 = labelArray[rand_2];
    [rand_label_1 setText:@"2"];
    [rand_label_2 setText:@"2"];
    
}

@end
