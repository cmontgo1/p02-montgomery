//
//  AppDelegate.h
//  p02-montgomery
//
//  Created by Montgomery Chelsea on 2/4/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

