//
//  ViewController.h
//  p02-montgomery
//
//  Created by Montgomery Chelsea on 2/4/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


//Give names to each of the square labels
@property(nonatomic, strong) IBOutlet UILabel *sq_1;
@property(nonatomic, strong) IBOutlet UILabel *sq_2;
@property(nonatomic, strong) IBOutlet UILabel *sq_3;
@property(nonatomic, strong) IBOutlet UILabel *sq_4;
@property(nonatomic, strong) IBOutlet UILabel *sq_5;
@property(nonatomic, strong) IBOutlet UILabel *sq_6;
@property(nonatomic, strong) IBOutlet UILabel *sq_7;
@property(nonatomic, strong) IBOutlet UILabel *sq_8;
@property(nonatomic, strong) IBOutlet UILabel *sq_9;
@property(nonatomic, strong) IBOutlet UILabel *sq_10;
@property(nonatomic, strong) IBOutlet UILabel *sq_11;
@property(nonatomic, strong) IBOutlet UILabel *sq_12;
@property(nonatomic, strong) IBOutlet UILabel *sq_13;
@property(nonatomic, strong) IBOutlet UILabel *sq_14;
@property(nonatomic, strong) IBOutlet UILabel *sq_15;
@property(nonatomic, strong) IBOutlet UILabel *sq_16;

//Give names to each of the buttons
@property(nonatomic, strong) IBOutlet UIButton *up_button;
@property(nonatomic, strong) IBOutlet UIButton *down_button;
@property(nonatomic, strong) IBOutlet UIButton *left_button;
@property(nonatomic, strong) IBOutlet UIButton *right_button;

//Restart button
@property(nonatomic, strong) IBOutlet UIButton *restart_button;

//Score labels
@property(nonatomic, strong) IBOutlet UILabel *high_score;
@property(nonatomic, strong) IBOutlet UILabel *current_score;

//Game over label
@property(nonatomic, strong) IBOutlet UILabel *game_over_label;

//The functions to call for each of the buttons being pressed
-(IBAction)up_click:(id)sender;
-(IBAction)down_click:(id)sender;
-(IBAction)left_click:(id)sender;
-(IBAction)right_click:(id)sender;

//Function for the restart button
-(IBAction)restart_click:(id)sender;

@end

